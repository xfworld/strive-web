import Vue from 'vue'
import App from './App'
import router from './common/router'
import store from './common/store'
import './common/axios'
import './common/utils'
import './common/config'
import './common/http'
import './common/components'

import 'font-awesome/css/font-awesome.css'
import 'datejs'

import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(ViewUI)

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

import ECharts from 'vue-echarts'
import 'echarts'
import 'echarts/theme/macarons'
Vue.component('e-charts', ECharts)

import VideoPlayer from 'vue-video-player'
import 'videojs-contrib-hls'
require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
Vue.use(VideoPlayer)

Vue.config.productionTip = false

const app = new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')

export default app

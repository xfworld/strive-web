import Vue from 'vue'

import mainLayout from '../components/main-layout'
Vue.component('main-layout', mainLayout)

import upload from '../components/upload'
Vue.component('upload', upload)
import Vue from 'vue'
import VueRouter from 'vue-router'
import config from './config'

import template_content from '@/pages/template/content'
import template_index from '@/pages/template/index'

import register from '@/pages/register/index'
import login from '@/pages/login/index'
import index from '@/pages/index/index'
import org from '@/pages/org/index'
import user from '@/pages/user/index'
import role_index from '@/pages/role/index'
import role_role from '@/pages/role/role'
import role_auth from '@/pages/role/auth'
import role_menu from '@/pages/role/menu'
import role_element from '@/pages/role/element'
import role_resource from '@/pages/role/resource'
import system_index from '@/pages/system/index'
import system_dict from '@/pages/system/dict'
import system_app from '@/pages/system/app'
import system_holiday from '@/pages/system/holiday'
import system_childDict from '@/pages/system/childDict'
import sched_index from '@/pages/sched/index'
import sched_executor from '@/pages/sched/executor'
import sched_job from '@/pages/sched/job'
import sched_log from '@/pages/sched/log'
import rule_index from '@/pages/rule/index'
import rule_definition from '@/pages/rule/rule_definition'
import rule_test from '@/pages/rule/rule_test'
import workflow_index from '@/pages/workflow/index'
import workflow_deployment from '@/pages/workflow/deployment'
import workflow_definition from '@/pages/workflow/definition'
import workflow_tasklist from '@/pages/workflow/tasklist'
import workflow_bpmn from '@/pages/workflow/bpmn-modeler'
import log_index from '@/pages/log/index'
import log_action from '@/pages/log/action'
import log_access from '@/pages/log/access'
import log_error from '@/pages/log/error'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'hash',
	routes: [{
		path: '/template_content',
		component: template_content,
		name: 'template_content'
	}, {
		path: '/template_index',
		component: template_index,
		name: 'template_index'
	}, {
		path: '/register',
		component: register,
		name: 'register'
	}, {
		path: '/login',
		component: login,
		name: 'login'
	}, {
		path: '/',
		component: index,
		name: '',
		children: [{
			path: '',
			component: org,
			name: 'org'
		}, {
			path: 'user',
			component: user,
			name: 'user'
		}, {
			path: 'role',
			component: role_index,
			name: 'role',
			children: [{
				path: 'role_role',
				component: role_role,
				name: 'role_role'
			},{
				path: 'role_auth',
				component: role_auth,
				name: 'role_auth'
			},{
				path: 'role_menu',
				component: role_menu,
				name: 'role_menu'
			},{
				path: 'role_element',
				component: role_element,
				name: 'role_element'
			},{
				path: 'role_resource',
				component: role_resource,
				name: 'role_resource'
			}]
		}, {
			path: 'system',
			component: system_index,
			name: 'system',
			children: [{
				path: 'system_dict',
				component: system_dict,
				name: 'system_dict'
			},{
				path: 'system_app',
				component: system_app,
				name: 'system_app'
			},{
				path: 'system_holiday',
				component: system_holiday,
				name: 'system_holiday'
			},{
				path: 'system_childDict',
				component: system_childDict,
				name: 'system_childDict'
			}]
		}, {
			path: 'sched',
			component: sched_index,
			name: 'sched',
			children: [{
				path: 'sched_executor',
				component: sched_executor,
				name: 'sched_executor'
			}, {
				path: 'sched_job',
				component: sched_job,
				name: 'sched_job'
			}, {
				path: 'sched_log',
				component: sched_log,
				name: 'sched_log'
			}]
		}, {
			path: 'rule_index',
			component: rule_index,
			name: 'rule',
			children: [{
				path: 'rule_definition',
				component: rule_definition,
				name: 'rule_definition'
			}, {
				path: 'rule_test',
				component: rule_test,
				name: 'rule_test'
			}]
		}, {
			path: 'workflow',
			component: workflow_index,
			name: 'workflow',
			children: [{
				path: 'workflow_deployment',
				component: workflow_deployment,
				name: 'workflow_deployment'
			}, {
				path: 'workflow_definition',
				component: workflow_definition,
				name: 'workflow_definition'
			}, {
				path: 'workflow_tasklist',
				component: workflow_tasklist,
				name: 'workflow_tasklist'
			}, {
				path: 'workflow_bpmn',
				component: workflow_bpmn,
				name: 'workflow_bpmn'
			}]
		}, {
			path: 'log',
			component: log_index,
			name: 'log',
			children: [{
				path: 'action',
				component: log_action,
				name: 'log_action'
			}, {
				path: 'access',
				component: log_access,
				name: 'log_access'
			}, {
				path: 'error',
				component: log_error,
				name: 'log_error'
			}]
		}]
	}]
});

/**
 * 导航守卫
 */
router.beforeEach((to, from, next) => {
	if (to.name != 'login' && to.name != 'register') {
		let token = localStorage.getItem(config.tokenName)
		if (!token) {
			next({name: 'login', replace: true})
			return;
		}
	}
	next()
});

export default router;
